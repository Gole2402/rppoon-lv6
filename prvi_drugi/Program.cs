﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prvi_drugi
{
    class Program
    {
        static void Main(string[] args)
        {
            Notebook notebook = new Notebook();
            notebook.AddNote(new Note("RPPOON", "Test"));
            notebook.AddNote(new Note("RPPOON-LV", "TEST!"));
            notebook.AddNote(new Note("RPPOON-LV6", "TEST_LV6"));

            IAbstractIterator iterator = notebook.GetIterator();        

            while (iterator.IsDone != true)
            {
                iterator.Current.Show();
                iterator.Next();
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prvi_drugi
{
    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treci_cetvrti
{
    class BankMemento
    {
        public string ownerName { get; private set; }
        public string ownerAddress { get; private set; }
        public decimal balance { get; private set; }

        public BankMemento(string ownername, string owneraddress, decimal balance)
        {
            this.ownerName = ownername; this.ownerAddress = owneraddress; this.balance = balance;
        }
        public BankMemento(BankAccount account)
        {
            this.ownerName = account.OwnerName; this.ownerAddress = account.OwnerAddress; this.balance = account.Balance;
        }
        public void AddPreviousState(BankAccount account)
        {
            balance = account.Balance;
        }
    }
}

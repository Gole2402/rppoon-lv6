﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace treci_cetvrti
{
    class Program
    {
        static void Main(string[] args)
        {
            CareTaker careTaker = new CareTaker();
            ToDoItem toDo = new ToDoItem("naslov1", "spavanje", new DateTime(2018, 1, 1));
            careTaker.AddPreviousState(toDo.StoreState());
            toDo.ChangeTask("idi u trgovinu");
            toDo.Rename("Novi naslov2");
            careTaker.AddPreviousState(toDo.StoreState());
            Console.WriteLine(toDo.ToString());
            toDo.RestoreState(careTaker.delete());
            Console.WriteLine(toDo.ToString());
            toDo.RestoreState(careTaker.Redo());
            Console.WriteLine(toDo.ToString());

            CareTaker bankCare = new CareTaker();
            BankAccount acc = new BankAccount("Dino", "Đakovo 123", 300);
            BankMemento memento = new BankMemento(acc);
            bankCare.PreviousState = memento;
            Console.WriteLine(bankCare.PreviousState.balance.ToString());

            acc.UpdateBalance(500);
            memento.AddPreviousState(acc);
            bankCare.PreviousState = memento;
            Console.WriteLine(bankCare.PreviousState.balance.ToString());



        }
    }
}
